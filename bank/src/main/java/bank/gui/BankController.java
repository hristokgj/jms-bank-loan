package bank.gui;

import bank.gateway.BankBrokerGateway;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import models.BankInterestReply;
import models.BankInterestRequest;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.net.URL;
import java.util.ResourceBundle;

public class BankController implements Initializable {
    private String BANK_ID;

    public ListView<ListViewLine> lvBankRequestReply;
    public TextField tfInterest;

    private BankBrokerGateway bankBrokerGateway;


    @FXML
    public void btnSendBankInterestReplyClicked() throws JMSException {
        String input = tfInterest.getText();
        try {
            double interest = Double.parseDouble(tfInterest.getText());
            BankInterestReply bankInterestReply = new BankInterestReply(interest, BANK_ID);
            ListViewLine listViewLine = lvBankRequestReply.getSelectionModel().getSelectedItem();
            int selectedIndex = lvBankRequestReply.getSelectionModel().getSelectedIndex();
            if (listViewLine != null) {
                BankInterestRequest bankInterestRequest = listViewLine.getBankInterestRequest();
                listViewLine.setBankInterestReply(bankInterestReply);
                lvBankRequestReply.refresh();

                bankBrokerGateway.sendBankReply(bankInterestRequest, bankInterestReply);
            } else {
                System.out.println("listViewLine is Null");
            }
        } catch (Exception ex) {
            System.out.println("Could not parse input: " + input);
        }

    }

    /**
     * This method returns the line of lvMessages.
     *
     * @param bankInterestRequest
     * @return The ListView line of lvMessages which contains the given request
     */
    private ListViewLine getRequestReply(BankInterestRequest bankInterestRequest) {

        for (ListViewLine listViewLine : lvBankRequestReply.getItems()) {
            if (listViewLine.getBankInterestRequest().equals(bankInterestRequest)) {
                return listViewLine;
            }
        }

        return null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.BANK_ID = BankMain.BANK_ID;
        System.out.println(this.BANK_ID);
        try {
            bankBrokerGateway = new BankBrokerGateway(this.BANK_ID) {
                @Override
                public void onBankRequestReceived(BankInterestRequest bankInterestRequest) {
                    ListViewLine listViewLine = new ListViewLine(bankInterestRequest);
                    lvBankRequestReply.getItems().add(listViewLine);
                    lvBankRequestReply.refresh();
                }
            };
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
