package bank.gateway;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import models.BankInterestReply;
import models.BankInterestRequest;
import serializers.BankSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.HashMap;

public class BankBrokerGateway {
    private MessageSenderGateway messageSenderGateway;
    private MessageReceiverGateway messageReceiverGateway;
    private BankSerializer bankSerializer;

    private final String BROKER_BANK_RECEIVER = "BrokerBankReceiver";
    private String bankChannel;

    private final String AGGREGATION_ID_PROPERTY = "aggregationID";

    private HashMap<String, Message> MappingMessages;
    private HashMap<String, BankInterestRequest> MappingMessageIdToBankRequest;
    private HashMap<BankInterestRequest, BankInterestReply> MappingBankRequestToBankReply;
    private HashMap<BankInterestRequest, String> MappingBankRequestToMessageId;

    public BankBrokerGateway(String bankID) throws JMSException, NamingException {
        this.bankChannel = bankID + "-Bank";
        this.messageReceiverGateway = new MessageReceiverGateway(this.bankChannel);
        this.messageSenderGateway = new MessageSenderGateway(BROKER_BANK_RECEIVER);
        this.bankSerializer = new BankSerializer();

        MappingMessages = new HashMap<>();
        MappingMessageIdToBankRequest = new HashMap<>();
        MappingBankRequestToBankReply = new HashMap<>();
        MappingBankRequestToMessageId = new HashMap<>();

        this.messageReceiverGateway.setListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                // Expect BankRequests -> save message ids
                try {
                    String json = ((TextMessage) message).getText();
                    BankInterestRequest bankInterestRequest = bankSerializer.deserializeBankRequestFromJson(json);

                    MappingMessages.put(message.getJMSMessageID(), message);
                    MappingMessageIdToBankRequest.put(message.getJMSMessageID(), bankInterestRequest);
                    MappingBankRequestToMessageId.put(bankInterestRequest, message.getJMSMessageID());

                    onBankRequestReceived(bankInterestRequest);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void onBankRequestReceived(BankInterestRequest bankInterestRequest) {
        // To be implemented in client.
    }

    public void sendBankReply(BankInterestRequest bankInterestRequest, BankInterestReply bankInterestReply)
            throws JMSException {
        String requestMessageId = MappingBankRequestToMessageId.get(bankInterestRequest);
        Message requestMessage = MappingMessages.get(requestMessageId);
        String jsonifiedRequest = bankSerializer.serializeBankReplyToJson(bankInterestReply);

        MappingBankRequestToBankReply.put(bankInterestRequest, bankInterestReply);

        Message message = messageSenderGateway.createTextMessage(jsonifiedRequest, requestMessageId);
        // Set aggregation id.
        message.setStringProperty(AGGREGATION_ID_PROPERTY, requestMessage.getStringProperty(AGGREGATION_ID_PROPERTY));
        messageSenderGateway.sendMessage(message);
        MappingMessages.put(message.getJMSMessageID(), message);
    }


}
