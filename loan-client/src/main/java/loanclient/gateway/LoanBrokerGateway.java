package loanclient.gateway;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import models.LoanReply;
import models.LoanRequest;
import serializers.LoanSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.UUID;

public class LoanBrokerGateway {
    private MessageSenderGateway messageSenderGateway;
    private MessageReceiverGateway messageReceiverGateway;
    private LoanSerializer loanSerializer;

    private String LOAN_CLIENT_CHANNEL;
    private final String BROKER_RECEIVER_CHANNEL = "BrokerLoanReceiver";

    private HashMap<String, models.LoanRequest> MappingMessageIdToLoanRequest;
    private HashMap<models.LoanRequest, models.LoanReply> MappingLoanRequestToLoanReply;

    public LoanBrokerGateway() throws JMSException, NamingException {
        this.LOAN_CLIENT_CHANNEL = "LoanClient-" + UUID.randomUUID().toString();
        this.messageReceiverGateway = new MessageReceiverGateway(LOAN_CLIENT_CHANNEL);
        this.messageSenderGateway = new MessageSenderGateway(BROKER_RECEIVER_CHANNEL);
        this.loanSerializer = new LoanSerializer();

        MappingMessageIdToLoanRequest = new HashMap<>();
        MappingLoanRequestToLoanReply = new HashMap<>();

        this.messageReceiverGateway.setListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                // Expected Loan Reply - messageCorrelationId should be the original LoanRequest messageId
                try {
                    String json = ((TextMessage) message).getText();
                    LoanReply loanReply = loanSerializer.deserializeLoanReplyFromJson(json);
                    LoanRequest loanRequest = MappingMessageIdToLoanRequest.get(message.getJMSCorrelationID());
                    MappingLoanRequestToLoanReply.put(loanRequest, loanReply);

                    if (loanRequest == null) {
                        throw new Exception("Loan Request was not found for correlation id within mapping.");
                    }
                    onLoanReplyArrived(loanRequest, loanReply);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        });
    }

    public void applyForLoan(LoanRequest loanRequest) throws JMSException {
        String jsonifiedLoanRequest = this.loanSerializer.serializeLoanRequestToJson(loanRequest);
        Message message = this.messageSenderGateway.createTextMessage(
                jsonifiedLoanRequest, this.messageReceiverGateway.getDestination());
        this.messageSenderGateway.sendMessage(message);
        MappingMessageIdToLoanRequest.put(message.getJMSMessageID(), loanRequest);
    }

    public void onLoanReplyArrived(LoanRequest loanRequest, LoanReply loanReply) throws Exception {
        // Should be implemented within client.
    }
}
