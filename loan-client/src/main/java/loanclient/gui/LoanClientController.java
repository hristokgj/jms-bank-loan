package loanclient.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import loanclient.gateway.LoanBrokerGateway;
import models.LoanReply;
import models.LoanRequest;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoanClientController implements Initializable {


    LoanBrokerGateway chainedGateway;

    public TextField tfSsn;
    public TextField tfAmount;
    public TextField tfTime;
    public ListView<ListViewLine> lvLoanRequestReply;

    @FXML
    public void btnSendLoanRequestClicked() throws JMSException {
        // create the BankInterestRequest
        int ssn = Integer.parseInt(tfSsn.getText());
        int amount = Integer.parseInt(tfAmount.getText());
        int time = Integer.parseInt(tfTime.getText());
        LoanRequest loanRequest = new LoanRequest(ssn, amount, time);

        //create the ListView line with the request and add it to lvLoanRequestReply
        ListViewLine listViewLine = new ListViewLine(loanRequest);
        lvLoanRequestReply.getItems().add(listViewLine);

        chainedGateway.applyForLoan(loanRequest);
    }

    /**
     * This method returns the line of lvMessages.
     *
     * @param loanRequest
     * @return The ListView line of lvMessages which contains the given request
     */
    private ListViewLine getRequestReply(LoanRequest loanRequest) {

        for (ListViewLine listViewLine : lvLoanRequestReply.getItems()) {
            if (listViewLine.getLoanRequest().equals(loanRequest)) {
                return listViewLine;
            }
        }

        return null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tfSsn.setText("123456");
        tfAmount.setText("80000");
        tfTime.setText("30");

        try {
            chainedGateway = new LoanBrokerGateway() {
                @Override
                public void onLoanReplyArrived(LoanRequest loanRequest, LoanReply loanReply) throws Exception {
                    ListViewLine listViewLine = getRequestReply(loanRequest);
                    if (listViewLine == null) {
                        throw new Exception("Did not find List view line for loan request");
                    }
                    listViewLine.setLoanReply(loanReply);
                    lvLoanRequestReply.refresh();
                }
            };
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }
}
