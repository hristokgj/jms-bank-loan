package gateways;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class MessageSenderGateway {
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageProducer producer;

    public Destination getDestination() {
        return destination;
    }

    public MessageSenderGateway(String channelName) throws NamingException, JMSException {
        Properties props = new Properties();
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        props.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
        props.put(("queue." + channelName), channelName);

        Context jndiContext = new InitialContext(props);
        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

        this.connection = connectionFactory.createConnection();
        this.session = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.destination = (Destination) jndiContext.lookup(channelName);
        this.producer = this.session.createProducer(null);
    }

    public Message createTextMessage(String textBody) throws JMSException {
        return this.session.createTextMessage(textBody);
    }

    public Message createTextMessage(String textBody, Destination returnAddress) throws JMSException {
        Message message = this.session.createTextMessage(textBody);
        message.setJMSReplyTo(returnAddress);
        return message;
    }

    public Message createTextMessage(String textBody, String correlationId)
            throws JMSException {
        Message message = this.session.createTextMessage(textBody);
        message.setJMSCorrelationID(correlationId);
        return message;
    }

    public Message createTextMessage(String textBody, Destination returnAddress, String correlationId)
            throws JMSException {
        Message message = this.session.createTextMessage(textBody);
        message.setJMSReplyTo(returnAddress);
        message.setJMSCorrelationID(correlationId);
        return message;
    }

    public void sendMessage(Message msg) throws JMSException {
        this.producer.send(this.destination, msg);
    }

    public void sendMessage(Message msg, String channelName) throws NamingException, JMSException {
        Properties props = new Properties();
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        props.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
        props.put(("queue." + channelName), channelName);

        Context jndiContext = new InitialContext(props);
        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destination = (Destination) jndiContext.lookup(channelName);
        MessageProducer producer = this.session.createProducer(destination);

        producer.send(msg);
    }

    public void sendMessage(Message msg, Destination destination) throws JMSException {
        this.producer.send(destination, msg);
    }
}
