package gateways;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class MessageReceiverGateway {
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageConsumer consumer;

    public Destination getDestination() {
        return destination;
    }

    public MessageReceiverGateway(String channelName) throws NamingException, JMSException {
        Properties props = new Properties();
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        props.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
        props.put(("queue." + channelName), channelName);

        Context jndiContext = new InitialContext(props);
        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

        this.connection = connectionFactory.createConnection();
        this.session = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.destination = (Destination) jndiContext.lookup(channelName);
        this.consumer = session.createConsumer(this.destination);
    }

    public void setListener(MessageListener messageListener) throws JMSException {
        // Start connection only after listener has been attached.
        this.consumer.setMessageListener(messageListener);
        this.connection.start();
    }
}
