package gateways;

import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class RestfulServiceGateway {

    private URI baseURI;
    private Client client;
    private WebTarget serviceTarget;

    public RestfulServiceGateway(String uriString) {
        this.baseURI = UriBuilder.fromUri(uriString).build();
        this.client = ClientBuilder.newClient(new ClientConfig());
        this.serviceTarget = client.target(baseURI);
    }

    public Response getJSONResponseForEndpoint(String endpointPath) {
        Invocation.Builder requestBuilder = this.serviceTarget.path(endpointPath).request().accept(
                MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();
        return response;
    }

    public Response postEntityToEndpoint(Entity entity, String endpointPath) {
        Invocation.Builder requestBuilder = this.serviceTarget.path(endpointPath).request().accept(
                MediaType.TEXT_PLAIN);
        Response response = requestBuilder.post(entity);
        return response;
    }

    public static Entity createJsonEntityForObject(Object o) {
        Entity entity = Entity.entity(o, MediaType.APPLICATION_JSON);
        return entity;
    }
}
