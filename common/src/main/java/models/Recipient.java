package models;

import net.sourceforge.jeval.Evaluator;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.util.ArrayList;

public class Recipient {
    private String jevalRule;
    private Evaluator evaluator;
    private String channelName;

    public String getChannelName() {
        return channelName;
    }

    public Recipient(String channelName, String jevalRule) throws JMSException, NamingException {
        this.channelName = channelName;
        this.jevalRule = jevalRule;
        this.evaluator = new Evaluator();
    }

    public Boolean evaluateSelfRule(ArrayList<String[]> evaluationVariables) throws Exception {
        for (String[] variableArray : evaluationVariables) {
            if (variableArray.length != 2) {
                throw new Exception("Expected variable array must be 2 variables: [{VariableName}, {VariableValue}]");
            }
            this.evaluator.putVariable(variableArray[0], variableArray[1]);
        }
        String result = this.evaluator.evaluate(this.jevalRule);
        return result.equals("1.0");
    }
}
