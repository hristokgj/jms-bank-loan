package models;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.HashMap;

public class RecipientList {

    private HashMap<String, Recipient> recipients;

    public RecipientList() {
        this.recipients = new HashMap<>();
    }

    public void addRecipient(String channelName, String jevalRule) throws NamingException, JMSException {
        recipients.put(channelName, new Recipient(channelName, jevalRule));
    }

    public ArrayList<Recipient> getApplicableRecipients(ArrayList<String[]> evaluationVariables)
            throws Exception {
        ArrayList<Recipient> applicableRecipients = new ArrayList<>();
        for (Recipient recipient : recipients.values()) {
            if (recipient.evaluateSelfRule(evaluationVariables)) {
                applicableRecipients.add(recipient);
            }
        }
        return applicableRecipients;
    }
}
