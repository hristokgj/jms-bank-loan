package serializers;

import com.owlike.genson.Genson;
import models.LoanReply;
import models.LoanRequest;

public class LoanSerializer {
    private Genson genson;

    public LoanSerializer() {
        this.genson = new Genson();
    }

    public String serializeLoanRequestToJson(LoanRequest loanRequest) {
        return this.genson.serialize(loanRequest);
    }

    public LoanRequest deserializeLoanRequestFromJson(String json) {
        return genson.deserialize(json, LoanRequest.class);
    }

    public String serializeLoanReplyToJson(LoanReply loanReply) {
        return this.genson.serialize(loanReply);
    }

    public LoanReply deserializeLoanReplyFromJson(String json) {
        return genson.deserialize(json, LoanReply.class);
    }
}
