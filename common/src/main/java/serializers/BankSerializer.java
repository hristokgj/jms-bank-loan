package serializers;

import com.owlike.genson.Genson;
import models.BankInterestReply;
import models.BankInterestRequest;

public class BankSerializer {
    private Genson genson;

    public BankSerializer() {
        this.genson = new Genson();
    }

    public String serializeBankRequestToJson(BankInterestRequest bankInterestRequest) {

        return this.genson.serialize(bankInterestRequest);
    }

    public BankInterestRequest deserializeBankRequestFromJson(String json) {
        return genson.deserialize(json, BankInterestRequest.class);
    }

    public String serializeBankReplyToJson(BankInterestReply bankInterestReply) {

        return this.genson.serialize(bankInterestReply);
    }

    public BankInterestReply deserializeBankReplyFromJson(String json) {

        return genson.deserialize(json, BankInterestReply.class);
    }
}
