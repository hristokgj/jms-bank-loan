package brokerclient.models;

import models.BankInterestReply;
import models.BankInterestRequest;
import models.Recipient;
import models.RecipientList;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.HashMap;

public class ScatterGatherer {
    private RecipientList recipientList;
    private HashMap<String, BankReplyAggregator> MappingAggregations;

    private final String AGGREGATION_ID_PROPERTY = "aggregationID";

    public ScatterGatherer() {
        this.recipientList = new RecipientList();
        MappingAggregations = new HashMap<>();
    }

    public void addRecipient(String channelName, String jevalRule) throws JMSException, NamingException {
        this.recipientList.addRecipient(channelName, jevalRule);
    }

    public BankReplyAggregator createAggregatorForRequest(BankInterestRequest bankInterestRequest) throws Exception {
        ArrayList<String[]> evaluationVariables = new ArrayList<>();
        String[] amountVariables = new String[]{"amount", Integer.toString(bankInterestRequest.getAmount())};
        String[] timeVariables = new String[]{"time", Integer.toString(bankInterestRequest.getTime())};
        evaluationVariables.add(amountVariables);
        evaluationVariables.add(timeVariables);

        ArrayList<Recipient> applicableRecipients = this.recipientList.getApplicableRecipients(evaluationVariables);

        BankReplyAggregator bankReplyAggregator = new BankReplyAggregator(bankInterestRequest) {
            @Override
            public void onBankReplyRetrievedFromAggregation(BankInterestReply bankInterestReply) {
                BankInterestRequest relatedRequest = this.getRelatedRequest();
                onBankReplyReturnedFromAggregator(bankInterestReply, relatedRequest);
            }
        };

        MappingAggregations.put(bankReplyAggregator.getId(), bankReplyAggregator);

        for (Recipient recipient : applicableRecipients) {
            bankReplyAggregator.addBankRecipient(recipient);
        }

        return bankReplyAggregator;
    }

    public void addReplyToAggregator(BankInterestReply bankInterestReply, String aggregatorID) throws Exception {
        BankReplyAggregator aggregator = MappingAggregations.get(aggregatorID);
        if (aggregator == null) {
            throw new Exception("Aggregator was not found for id: " + aggregatorID);
        }
        aggregator.addBankReply(bankInterestReply);
    }

    public void onBankReplyReturnedFromAggregator(
            BankInterestReply bankInterestReply, BankInterestRequest bankInterestRequest) {
        // Callback to be implemented by gateway
    }
}
