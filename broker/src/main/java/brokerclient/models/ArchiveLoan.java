package brokerclient.models;

public class ArchiveLoan {
    private int SSN;
    private int amount; // the ammount to borrow
    private int time; // the time-span of the loan in years
    private double interest; // the interest that the bank offers for the requested loan
    private String bank; // the unique quote identification of the bank which makes the offer

    public ArchiveLoan(int SSN, int amount, int time, double interest, String bank) {
        this.SSN = SSN;
        this.amount = amount;
        this.time = time;
        this.interest = interest;
        this.bank = bank;
    }

    public ArchiveLoan() {
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
}
