package brokerclient.models;

import models.BankInterestReply;
import models.BankInterestRequest;
import models.Recipient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class BankReplyAggregator {
    private String id;
    private ArrayList<BankInterestReply> replies;
    private HashMap<String, Recipient> MappingChannelNameToRecipient;
    private BankInterestRequest relatedRequest;


    public String getId() {
        return id;
    }

    public BankInterestRequest getRelatedRequest() {
        return relatedRequest;
    }

    public int getRecipientCount() {
        return MappingChannelNameToRecipient.size();
    }

    public BankReplyAggregator(BankInterestRequest relatedRequest) {
        this.id = UUID.randomUUID().toString();
        this.replies = new ArrayList<>();
        this.MappingChannelNameToRecipient = new HashMap<>();
        this.relatedRequest = relatedRequest;
    }

    public ArrayList<Recipient> getRecipients() {
        ArrayList<Recipient> recipients = new ArrayList<>();
        for (Recipient r : this.MappingChannelNameToRecipient.values()) {
            recipients.add(r);
        }
        return recipients;
    }

    public void addBankRecipient(Recipient recipient) {
        MappingChannelNameToRecipient.put(recipient.getChannelName(), recipient);
    }

    public void addBankReply(BankInterestReply bankInterestReply) {
        replies.add(bankInterestReply);

        if (MappingChannelNameToRecipient.size() == replies.size()) {
            this.retrieveBankReplyWithLowestPositiveInterest();
        }

    }

    private void retrieveBankReplyWithLowestPositiveInterest() {
        BankInterestReply lowestInterestReply = null;
        for (BankInterestReply bankInterestReply : this.replies) {
            if (bankInterestReply.getInterest() > 0) {
                if (lowestInterestReply == null) {
                    lowestInterestReply = bankInterestReply;
                } else if (bankInterestReply.getInterest() < lowestInterestReply.getInterest()) {
                    lowestInterestReply = bankInterestReply;
                }
            }
        }
        this.onBankReplyRetrievedFromAggregation(lowestInterestReply);
    }

    public void onBankReplyRetrievedFromAggregation(BankInterestReply bankInterestReply) {
        // To be implemented by container.
    }

}
