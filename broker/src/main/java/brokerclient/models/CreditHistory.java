package brokerclient.models;

public class CreditHistory {
    private int creditScore;
    private int history;

    public CreditHistory(int creditScore, int history) {
        this.creditScore = creditScore;
        this.history = history;
    }

    public CreditHistory() {

    }

    public int getCreditScore() {
        return creditScore;
    }

    public int getHistory() {
        return history;
    }

    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }

    public void setHistory(int history) {
        this.history = history;
    }
}
