package brokerclient.gui;

import brokerclient.enrichers.CreditAgencyContentEnricher;
import brokerclient.gateways.BrokerBankClientGateway;
import brokerclient.gateways.BrokerLoanClientGateway;
import brokerclient.models.ArchiveLoan;
import brokerclient.routers.LoanArchiveRouter;
import gateways.RestfulServiceGateway;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import models.BankInterestReply;
import models.BankInterestRequest;
import models.LoanReply;
import models.LoanRequest;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.ws.rs.client.Entity;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class BrokerClientController implements Initializable {

    public ListView<ListViewLine> lvLoanRequestReply;

    private BrokerLoanClientGateway brokerLoanClientGateway;
    private BrokerBankClientGateway brokerBankClientGateway;

    private CreditAgencyContentEnricher creditAgencyContentEnricher;
    private LoanArchiveRouter loanArchiveRouter;

    private HashMap<LoanRequest, BankInterestRequest> MappingLoanRequestToBankRequest;
    private HashMap<BankInterestRequest, LoanRequest> MappingBankRequestToLoanRequest;
    private HashMap<LoanRequest, LoanReply> MappingLoanRequestToLoanReply;
    private HashMap<BankInterestRequest, BankInterestReply> MappingBankRequestToBankReply;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        MappingLoanRequestToBankRequest = new HashMap<>();
        MappingBankRequestToLoanRequest = new HashMap<>();
        MappingLoanRequestToLoanReply = new HashMap<>();
        MappingBankRequestToBankReply = new HashMap<>();

        creditAgencyContentEnricher = new CreditAgencyContentEnricher();
        loanArchiveRouter = new LoanArchiveRouter();

        try {
            brokerLoanClientGateway = new BrokerLoanClientGateway(false) {
                @Override
                public void onLoanRequestArrived(LoanRequest loanRequest) {
                    // Create a Bank Request
                    BankInterestRequest bankInterestRequest = null;
                    try {
                        bankInterestRequest = creditAgencyContentEnricher.enrichAndConvertLoanRequestToBankRequest(
                                loanRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                    System.out.println(bankInterestRequest.toString());
                    // Update mappings
                    MappingLoanRequestToBankRequest.put(loanRequest, bankInterestRequest);
                    MappingBankRequestToLoanRequest.put(bankInterestRequest, loanRequest);

                    ListViewLine listViewLine = new ListViewLine(loanRequest);
                    lvLoanRequestReply.getItems().add(listViewLine);
                    lvLoanRequestReply.refresh();

                    // Send to bank.
                    // XXX: A problem might arise if the brokerLoanClientGateway is initialized, and the
                    // brokerBankClientGateway is not.
                    try {
                        brokerBankClientGateway.sendBankRequest(bankInterestRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.exit(1);
                    }

                }
            };

            brokerBankClientGateway = new BrokerBankClientGateway(false) {
                @Override
                public void onBankReplyArrived(BankInterestRequest bankInterestRequest,
                                               BankInterestReply bankInterestReply) {
                    try {
                        MappingBankRequestToBankReply.put(bankInterestRequest, bankInterestReply);
                        Entity e = RestfulServiceGateway.createJsonEntityForObject(bankInterestReply);
                        System.out.println(e.toString());

                        LoanReply loanReply = new LoanReply(
                                bankInterestReply.getInterest(), bankInterestReply.getQuoteId());

                        LoanRequest loanRequest = MappingBankRequestToLoanRequest.get(bankInterestRequest);

                        MappingLoanRequestToLoanReply.put(loanRequest, loanReply);

                        ListViewLine listViewLine = getRequestReply(loanRequest);
                        listViewLine.setLoanReply(loanReply);
                        lvLoanRequestReply.refresh();

                        ArchiveLoan archiveLoan = new ArchiveLoan(loanRequest.getSsn(), loanRequest.getAmount(),
                                loanRequest.getTime(), loanReply.getInterest(), loanReply.getBankID());
                        loanArchiveRouter.archiveLoan(archiveLoan);
                        brokerLoanClientGateway.sendLoanReply(loanReply, loanRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.exit(1);
                    }

                }
            };
            brokerLoanClientGateway.startListener();
            brokerBankClientGateway.startListener();
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method returns the line of lvMessages.
     *
     * @param loanRequest
     * @return The ListView line of lvMessages which contains the given request
     */
    private ListViewLine getRequestReply(LoanRequest loanRequest) {

        for (ListViewLine listViewLine : lvLoanRequestReply.getItems()) {
            if (listViewLine.getLoanRequest().equals(loanRequest)) {
                return listViewLine;
            }
        }

        return null;
    }
}
