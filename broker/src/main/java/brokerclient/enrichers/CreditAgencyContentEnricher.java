package brokerclient.enrichers;

import brokerclient.models.CreditHistory;
import gateways.RestfulServiceGateway;
import models.BankInterestRequest;
import models.LoanRequest;

import javax.ws.rs.core.Response;

public class CreditAgencyContentEnricher {

    private RestfulServiceGateway creditAgencyServiceGateway;
    private final String CREDIT_AGENCY_BASE_URI_STRING = "http://localhost:8080/credit/rest/";
    private final String CREDIT_AGENCY_HISTORY_ENDPOINT = "history/";

    public CreditAgencyContentEnricher() {
        this.creditAgencyServiceGateway = new RestfulServiceGateway(CREDIT_AGENCY_BASE_URI_STRING);
    }

    public BankInterestRequest enrichAndConvertLoanRequestToBankRequest(LoanRequest loanRequest) throws Exception {
        Response creditAgencyResponse = creditAgencyServiceGateway.getJSONResponseForEndpoint(
                CREDIT_AGENCY_HISTORY_ENDPOINT + loanRequest.getSsn());
        if (creditAgencyResponse.getStatus() == Response.Status.OK.getStatusCode()) {
            CreditHistory creditHistory = creditAgencyResponse.readEntity(CreditHistory.class);
            BankInterestRequest bankInterestRequest = new BankInterestRequest(
                    loanRequest.getAmount(), loanRequest.getTime(),
                    creditHistory.getCreditScore(), creditHistory.getHistory());
            return bankInterestRequest;
        } else {
            throw new Exception("Response status code was not 200, " + creditAgencyResponse.toString());
        }
    }
}
