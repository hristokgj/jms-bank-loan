package brokerclient.gateways;

import brokerclient.models.BankReplyAggregator;
import brokerclient.models.ScatterGatherer;
import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import models.BankInterestReply;
import models.BankInterestRequest;
import models.Recipient;
import serializers.BankSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class BrokerBankClientGateway {
    private MessageSenderGateway messageSenderGateway;
    private MessageReceiverGateway messageReceiverGateway;
    private BankSerializer bankSerializer;

    private final String BROKER_BANK_RECEIVER_CHANNEL = "BrokerBankReceiver";

    private final String AGGREGATION_ID_PROPERTY = "aggregationID";

    // Banks and Rules
    private final String ABN_BANK_CHANNEL = "ABN-Bank";
    private final String ABN_RECIEVE_RULE = "#{amount} >= 200000 && #{amount} <= 300000  && #{time} <= 20";
    private final String ING_BANK_CHANNEL = "ING-Bank";
    private final String ING_RECIEVE_RULE = "#{amount} <= 100000 && #{time} <= 10";
    private final String RABO_BANK_CHANNEL = "Rabo-Bank";
    private final String RABO_RECIEVE_RULE = "#{amount} <= 250000 && #{time} <= 15";

    private HashMap<String, BankInterestRequest> MappingMessageIdToBankRequest;
    private HashMap<String, Message> MappingMessages;
    private HashMap<BankInterestRequest, BankInterestReply> MappingBankRequestToBankReply;
    private HashMap<BankInterestReply, String> MappingBankInterestReplyToMessageIds;

    private ScatterGatherer scatterGatherer;

    public BrokerBankClientGateway(Boolean startListener) throws JMSException, NamingException {
        this.messageSenderGateway = new MessageSenderGateway(BROKER_BANK_RECEIVER_CHANNEL);
        this.messageReceiverGateway = new MessageReceiverGateway(BROKER_BANK_RECEIVER_CHANNEL);
        this.bankSerializer = new BankSerializer();

        MappingMessageIdToBankRequest = new HashMap<>();
        MappingMessages = new HashMap<>();
        MappingBankRequestToBankReply = new HashMap<>();
        MappingBankInterestReplyToMessageIds = new HashMap<>();

        this.scatterGatherer = new ScatterGatherer() {
            @Override
            public void onBankReplyReturnedFromAggregator(
                    BankInterestReply bankInterestReply, BankInterestRequest bankInterestRequest) {
                if (bankInterestReply == null) {
                    bankInterestReply = new BankInterestReply(-1, "Rejected");
                }
                MappingBankRequestToBankReply.put(bankInterestRequest, bankInterestReply);
                onBankReplyArrived(bankInterestRequest, bankInterestReply);
            }

//            @Override
//            public void onMessageSent(Message m, BankInterestRequest bankInterestRequest) {
//                try {
//                    MappingMessageIdToBankRequest.put(m.getJMSMessageID(), bankInterestRequest);
//                    MappingMessages.put(m.getJMSMessageID(), m);
//                } catch (JMSException e) {
//                    e.printStackTrace();
//                }
//            }
        };

        // ------------------------------------------------------------------------------------------------
        this.scatterGatherer.addRecipient(ABN_BANK_CHANNEL, ABN_RECIEVE_RULE);
        this.scatterGatherer.addRecipient(ING_BANK_CHANNEL, ING_RECIEVE_RULE);
        this.scatterGatherer.addRecipient(RABO_BANK_CHANNEL, RABO_RECIEVE_RULE);
        // ------------------------------------------------------------------------------------------------

        if (startListener) {
            this.startListener();
        }
    }

    public void startListener() throws JMSException {
        this.messageReceiverGateway.setListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                // Expected Bank replies with correct correlation ids attached.
                try {
                    System.out.println(message);
                    String json = ((TextMessage) message).getText();
                    BankInterestReply bankInterestReply = bankSerializer.deserializeBankReplyFromJson(json);

                    String aggregatorID = message.getStringProperty(AGGREGATION_ID_PROPERTY);

                    scatterGatherer.addReplyToAggregator(bankInterestReply, aggregatorID);

                    MappingBankInterestReplyToMessageIds.put(bankInterestReply, message.getJMSMessageID());
                    MappingMessages.put(message.getJMSMessageID(), message);

                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        });
    }

    public void onBankReplyArrived(BankInterestRequest bankInterestRequest, BankInterestReply bankInterestReply) {
        // implement by client
    }

    public void sendBankRequest(BankInterestRequest bankInterestRequest) throws Exception {

        BankReplyAggregator aggregator = this.scatterGatherer.createAggregatorForRequest(bankInterestRequest);

        String jsonifiedBankRequest = this.bankSerializer.serializeBankRequestToJson(bankInterestRequest);

        for (Recipient r : aggregator.getRecipients()) {
            Message m = this.messageSenderGateway.createTextMessage(jsonifiedBankRequest);
            m.setStringProperty(AGGREGATION_ID_PROPERTY, aggregator.getId());
            this.messageSenderGateway.sendMessage(m, r.getChannelName());
            // Add mappings
            MappingMessageIdToBankRequest.put(m.getJMSMessageID(), bankInterestRequest);
            MappingMessages.put(m.getJMSMessageID(), m);
        }

        if (aggregator.getRecipientCount() == 0) {
            // Request was not accepted by any bank, return null reply.
            TimeUnit.SECONDS.sleep(2);
            aggregator.onBankReplyRetrievedFromAggregation(null);
        }
    }

}
