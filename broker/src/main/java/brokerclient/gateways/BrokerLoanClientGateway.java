package brokerclient.gateways;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import models.LoanReply;
import models.LoanRequest;
import serializers.LoanSerializer;

import javax.jms.*;
import javax.naming.NamingException;
import java.util.HashMap;

public class BrokerLoanClientGateway {
    private MessageSenderGateway messageSenderGateway;
    private MessageReceiverGateway messageReceiverGateway;
    private LoanSerializer loanSerializer;

    private final String BROKER_CLIENT_RECEIVER_CHANNEL = "BrokerLoanReceiver";

    private HashMap<String, LoanRequest> MappingMessageIdToLoanRequest;
    private HashMap<LoanRequest, String> MappingLoanRequestToMessageId;
    private HashMap<String, Message> MappingMessages;
    private HashMap<models.LoanRequest, models.LoanReply> MappingLoanRequestToLoanReply;

    public BrokerLoanClientGateway(Boolean startListener) throws JMSException, NamingException {
        this.messageReceiverGateway = new MessageReceiverGateway(BROKER_CLIENT_RECEIVER_CHANNEL);
        // The sender is per client for which the channel is unknown.
        this.messageSenderGateway = new MessageSenderGateway(BROKER_CLIENT_RECEIVER_CHANNEL);
        this.loanSerializer = new LoanSerializer();

        MappingMessageIdToLoanRequest = new HashMap<>();
        MappingLoanRequestToMessageId = new HashMap<>();
        MappingMessages = new HashMap<>();
        MappingLoanRequestToLoanReply = new HashMap<>();

        if (startListener) {
            this.startListener();
        }

    }

    public void startListener() throws JMSException {
        this.messageReceiverGateway.setListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                // Expected - Loan request with returnAddress set.
                try {
                    String json = ((TextMessage) message).getText();
                    LoanRequest loanRequest = loanSerializer.deserializeLoanRequestFromJson(json);

                    // Update mappings.
                    MappingMessages.put(message.getJMSMessageID(), message);
                    MappingMessageIdToLoanRequest.put(message.getJMSMessageID(), loanRequest);
                    MappingLoanRequestToMessageId.put(loanRequest, message.getJMSMessageID());

                    onLoanRequestArrived(loanRequest);

                } catch (JMSException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        });
    }

    public void onLoanRequestArrived(LoanRequest loanRequest) {
        // Should be implemented within client.
    }

    public void sendLoanReply(LoanReply loanReply, LoanRequest loanRequest) throws JMSException {
        String requestMessageId = MappingLoanRequestToMessageId.get(loanRequest);
        Message originalRequestMessage = MappingMessages.get(requestMessageId);
        Destination messageSourceDestination = originalRequestMessage.getJMSReplyTo();

        String jsonifiedReply = loanSerializer.serializeLoanReplyToJson(loanReply);

        Message replyMessage = messageSenderGateway.createTextMessage(
                jsonifiedReply, messageSourceDestination, originalRequestMessage.getJMSMessageID());

        messageSenderGateway.sendMessage(replyMessage, messageSourceDestination);
        MappingMessages.put(replyMessage.getJMSMessageID(), replyMessage);
        MappingLoanRequestToLoanReply.put(loanRequest, loanReply);
    }
}
