package brokerclient.routers;

import brokerclient.models.ArchiveLoan;
import gateways.RestfulServiceGateway;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

public class LoanArchiveRouter {
    private RestfulServiceGateway creditAgencyServiceGateway;
    private final String ARCHIVE_BASE_URI_STRING = "http://localhost:8080/archive/rest/";
    private final String ARCHIVE_POST_ENDPOINT = "accepted/";

    public LoanArchiveRouter() {
        this.creditAgencyServiceGateway = new RestfulServiceGateway(ARCHIVE_BASE_URI_STRING);
    }

    public void archiveLoan(ArchiveLoan archiveLoan) throws Exception {
        if (archiveLoan.getInterest() > 0) {
            Entity loanReplyEntity = RestfulServiceGateway.createJsonEntityForObject(archiveLoan);
            Response r = creditAgencyServiceGateway.postEntityToEndpoint(loanReplyEntity, ARCHIVE_POST_ENDPOINT);

            if (r.getStatus() != Response.Status.NO_CONTENT.getStatusCode()) {
                throw new Exception("Response status code was not 204, " + r.toString());
            }
        }
    }


}
